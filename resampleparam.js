/*
1. Take the input time (in milliseconds) and resample/interpolate to 
   a fixed interval time-step

   1800 seconds = 30 mins
*/


var interpolateparam = function(param, timesecs, timestepsec) {
	var eidx = timesec.length;
	
	var f = createInterpolant(timesecs, param);
	for (var x = timesec[0]; x <= timesec[eidx]; x+=timestepsec) {
		resultParam.push(f(x));
		resultTime.push(x);
		
		return {
			Param: resultParam,
			Time: resultTime
		};
	};
};