<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<HTML> 
<HEAD>
	<TITLE>Water Surface Profile Webapp Development</TITLE>
	
	<!-- Include jQuery for themes and AJAX control -->
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>
	<script src="http://jquery.bassistance.de/validate/additional-methods.js"></script>
	<SCRIPT SRC="//code.jquery.com/ui/1.10.4/jquery-ui.js"></SCRIPT> 
	
	<!-- Load a style sheet -->
	<LINK HREF="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"
		 REL="stylesheet">

	<LINK HREF="style.css" REL="stylesheet">
	<!-- UI Tabs -->
	<SCRIPT>$(function() {$( "#tabs" ).tabs();});</SCRIPT> 
	<!-- drop down calendars -->
	<LINK HREF="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" REL="stylesheet">
	<SCRIPT>
	$(function() {
		$( "#startdatepicker" ).datepicker();
	});
	$(function() {
		$( "#enddatepicker" ).datepicker();
	});
	</SCRIPT> 
	<!-- Load Highcharts -->
	<SCRIPT SRC="./js/highcharts.js"></SCRIPT> 
	<SCRIPT SRC="./js/modules/exporting.js"></SCRIPT>

	<!-- User Scripts -->
	<SCRIPT SRC="resampleparam.js" TYPE="text/javascript"> </SCRIPT>
	
	<script type="text/javascript">
	function showloading() {
	  $('#content').hide();
	  $('#loading').show();
	}

	function hideloading() {
	  $('#content').show();
	  $('#loading').hide();
	}
	$(document).ready(function(){
		$("#InputForm").validate({
			debug: true,
			rules: {
				reference_hydrograph: {
					required: true,
					minlength: 8,
					digits: true
					},
				start_date: {
					required: true,
					date: true
					},
				end_date: {
					required: true,
					date: true
					},
				profile_stations: {
					required: true,
					},
			},
			messages: {
				reference_hydrograph: "Must be a valid USGS Station Number.",
				start_date: "Must be a valid mm/dd/yyyy date.",
				start_date: "Must be a valid mm/dd/yyyy date.",
				profile_stations: "Required.",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				//$('#content').hide();
				//$('#loading').show();
				$("#loading").fadeIn();
				$.post('processform.php', $("#InputForm").serialize(), function(data) {
					$("#tabs-2").html(data);
					$("#loading").fadeOut();
				});
				
				var selected = $("#tabs").tabs("option", "active");
				$("#tabs").tabs("option", "active", selected + 1);
				$('#content').show();
				$('#loading').hide();
			}
		});
	});
	</script>
</HEAD>

<body>
	<!-- Loading animation -->
	<div id="loading">
	  <span class="helper"></span><img id="loading-image" src="ajax-loader.gif" alt="Loading..." /><p>Processing, please wait...</p>
	</div>
	
	
	<DIV ID="content"> 
	<H1>Water Surface Profile Webapp (development alpha)</H1> 

	<!-- TAB CONTROL -->
	<DIV ID="tabs"> 
	<UL>
	<LI><A HREF="#tabs-1">Setup Application</A></LI> 
	<LI><A HREF="#tabs-2">Plotting</A></LI> 
	<LI><A HREF="#tabs-3">Reserved</A></LI> 
	</UL>

<DIV ID="tabs-1"> 
	<FORM ACTION="#" METHOD="POST" Name="InputForm" id="InputForm">
	<FIELDSET> <LEGEND>Input Parameters</LEGEND> 
	<UL CLASS="form-fields">
	<LI><LABEL FOR="reference_hydrograph">Reference Station</LABEL>
	<INPUT CLASS="text-input" ID="reference_hydrograph"
		  NAME="reference_hydrograph" TYPE="text" value="05536890"> </LI>
	<LI><LABEL FOR="start_date">Start Date</LABEL> 
	<INPUT CLASS="text-input" ID="startdatepicker"
		  NAME="start_date" TYPE="text" value="04/15/2013"> </LI>
	<LI><LABEL FOR="end_date">End Date</LABEL> 
	<INPUT CLASS="text-input" ID="enddatepicker" NAME="end_date"
		  TYPE="text" value="04/21/2013"> </LI>
	<LI>

	<P CLASS="form-help">Input the USGS station numbers and
	river mile information following the format example (csv coordinate pairs). List
	stations in ascending order by river mile.</P> 
	<LABEL FOR="profile_stations">Stations</LABEL> 
	<TEXTAREA CLASS="text-input" ID="profile_stations" NAME="profile_stations" TYPE="text" COLS="20" Rows="5">
05536998,291.1,
05536890,303.0,
05536118,325.9,
05536123,326.4,
05536121,326.7,
04087440,326.8
	</TEXTAREA></LI>
	<button TYPE="submit" VALUE="Submit" ID="submitbtn">Submit</button>
	<button TYPE="reset" VALUE="Reset" ID="resetbtn">Clear Form</button>
	</UL>
	</FIELDSET>
	</form>
</div> <!-- tabs-1-->

<DIV ID="tabs-2">
</div> <!-- tabs-2-->

<DIV ID="tabs-3">
</div> <!-- tabs-2-->
</body>