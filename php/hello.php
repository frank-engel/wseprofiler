<html>
<head>
<title>PHP Testing</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
</head>
<body>
<h1>Basic hello world</h1>
<?php echo '<p>Hello World</p>'; ?> 
 
<br>
<hr>

<h1>Testing loading NWIS Data and Parsing</h1>
<p>Below, I am inputing some RDB formmated data from NWIS, and trying to get it into 
functional arrays that I can pass to other functions, write files, and/or pass to javascript.</p>

<?php
// Increase the memory limit to help avoid
// out of memory errors
ini_set('memory_limit', '1024M'); 

// Read some RDB format data using a URL query to NWIS Web (2 parameter)
$rdb=file_get_contents("http://nwis.waterdata.usgs.gov/usa/nwis/uv?cb_00065=on&cb_00060=on&format=rdb&period=&begin_date=2012-12-03&end_date=2012-12-5&site_no=05536890&referred_module=sw");

// Separate the data into an Array of rows
$rows        = explode("\n", $rdb);

// Parse the RDB, and deal to variables
// First, traverse the header. In no case should the header
// be longer than 100 rows, so use that as the max for loop size
$maxHeaderSize = 100;
for ($offset = 0; $offset <= $maxHeaderSize; $offset++) {
    // Take header row by row. Stop when the first character 
	// is no longer "#"
	$irow = trim($rows[$offset]);
	$irow = preg_replace('/\s+/',' ',$irow);
	$icol = explode(" ", $irow);
	if (!in_array("#",$icol)) {
		break; 
	} 
}

// Segment the RDB Data, Labels, and Comment Codes
$colnames = array_slice($rows,$offset,1);
$colcomments = array_slice($rows,$offset+1,1);
$data = array_slice($rows,$offset+2);

echo '<p> The loaded RDB webquery contains the following columns:</p>';
echo "<pre>";
print_r($colnames);
echo "</pre>";
//print_r($colcomments);
//print_r($data);

// Parse each row
$i = 0;
foreach($data as $key => $value){
    $cols[$i] = explode("\t", $value);
    $i++;
}


// Extract discharge and time
$i = 0;

$rawtime   = array();
$discharge = array();
foreach ($cols as $key1 => $value1) {
	foreach ($value1 as $key2 => $value2) {
		if ($key2 == 2 && !empty($value2)) {
			$rawtime[$i] = $value2;
		}
		if ($key2 == 6 && !empty($value2)) {
			$discharge[$i] = $value2;
		$i++;
		}
	}
}
if (count($rawtime)>count($discharge)){
	array_pop($rawtime);
}

$i = count($rawtime);
$times = array();
for ($ii = 0; $ii <= $i; $ii++){
	$times[$ii] = strtotime($rawtime[$ii]);
}


/*
echo '<p>The parsed array:</p>';
echo "<pre>";
print_r($times); //explore results
echo "</pre>";
*/
?>

<hr>
<h1> Let's try to make a hydrograph of the loaded data</h1>
<script src="../js/highcharts.js"></script>
<script src="../js/modules/exporting.js"></script>
<script type="text/javascript">
	$(function () {
		$('#container').highcharts({
			series: [{
				name: 'Q',
				data: [<?php echo implode(',', $discharge); ?>],
			}]
		});
	});
</script>

<div id="container" style="height: 300px"></div>
 </body>
</html>