<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>sample plot</title>
<style>
div{
	height:600px;
	width:800px;
	border-left:thin #000 solid;
	border-bottom:thin #000 solid;
	position:relative;
}
div .dot{
	position:absolute;
}
</style>
</head>
<body>

<?php
$gagefile=file_get_contents("http://waterdata.usgs.gov/nwis/dv?cb_00065=on&cb_00060=on&format=rdb&period=&begin_date=2008-12-03&end_date=2008-12-25&site_no=05550000&referred_module=sw");
$data = explode("	",$gagefile);
$date = array();
$gageheight = array();
$discharge = array();
$l=0;
for($i=14;$i<count($data);$i=$i+6){
	$date[$l] = $data[$i];
	$gageheight[$l] = $data[$i+1];
	$discharge[$l] = $data[$i+3];
	$l=$l+1;
}
for($i=0;$i<count($discharge);$i++){
	$discharge[$i]=substr($discharge[$i],0,-1);
}
?>

<h2>Selected Reference Hydrograph</h2>
<div>
<?php
for($i=0;$i<count($discharge);$i++){
	$left=$i*40;
	echo '<img class="dot" src="plot.gif" style="top:'.$discharge[$i].'px;left:'.$left.'px;" />';
}
?>
</div>
</body>
</html>
