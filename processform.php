<?php
	// Validate the form data and reformat date string
	function test_input($formdata)
	{
	  $formdata=trim($formdata);
	  $formdata=stripslashes($formdata);
	  $formdata=htmlspecialchars($formdata);
	  return $formdata;
	}

	// define variables and set to empty values
	$station=$startdate=$enddate=$profilestations="";

	if ($_SERVER["REQUEST_METHOD"]=="POST")
	{
	  $station=test_input($_POST["reference_hydrograph"]);
	  $startdate=test_input($_POST["start_date"]);
	  $enddate=test_input($_POST["end_date"]);
	  $profilestations=test_input($_POST["profile_stations"]);
	  
	};
	
	// Reformat the dates
	$startdate=str_replace("/","-",$startdate);
	$enddate=str_replace("/","-",$enddate);

	// Query NWIS for Reference Station Discharge
	$parameter = "cb_00060";  // discharge
	include 'nwisrequest.php';
	list($refQtime, $discharge) = read_nwis($station,$startdate,$enddate,$parameter);
	
	// Parse the input WSE Stations and query NWIS
	$profilestations = explode(',', $profilestations);
	$size = count($profilestations);
	$rsta = array(); $rmile = array();
	for ($i = 0; $i < $size; $i += 2) {
	  $rsta[]  = trim($profilestations[$i]);
	  $rmile[] = trim($profilestations[$i+1]);
	}

	$parameter = "cb_00065";  // stage
	$size   = count($rsta);
	$rtime  = array();
	$rstage = array();

	for($i=0; $i < $size; $i++){
		list($rtime[$rsta[$i]],$rstage[$rsta[$i]]) = read_nwis($rsta[$i],$startdate,$enddate,$parameter);
	}
	
	function toFloats($array) {
    return array_map('floatval', $array);
	}
	$rtime  = array_map('toFloats', $rtime);
	$rstage = array_map('toFloats', $rstage);
		
	// Create flat arrays containing the data for each WSE station
    for ($i = 0; $i < $size; $i++){
		${"rtime".$rsta[$i]}  = array_values($rtime[$rsta[$i]]);
		${"rstage".$rsta[$i]} = array_values($rstage[$rsta[$i]]);
	}
	
	// TEMP: Convert Lemont to CCD
	$ii = count($rstage05536890);
	for ($i = 0; $i < $ii; $i++){
		$rstage05536890[$i] = ((552.049 + $rstage05536890[$i]) - 579.48);
	}
				
	?> 
<form>
<FIELDSET> <LEGEND>Reference Hydrograph</LEGEND> 
<DIV ID="highchart-refhydro">
<script type="text/javascript" src="createInterpolant.js"></script>
	<script>
	// Interpolate a consistent timestep (30 minutes)
	var param = <?php  echo "[" . implode(',', $discharge) . "]";?>;
	var timesecs = <?php  echo "[" . implode(',', $refQtime) . "]"; ?>;
	var timestepsec = 1800;
	var interpolateparam = function(param, timesecs, timestepsec) {
		var eidx = timesecs.length -1; // zero based indexing
		
		var f = createInterpolant(timesecs, param);
		var resultParam = []; resultTime = [];
		for (var x = timesecs[0]; x <= timesecs[eidx]; x+=timestepsec) {
			resultParam.push(f(x));
			resultTime.push(x);
		};
		return {
				Param: resultParam,
				Time: resultTime
			};
	};
	var Q = interpolateparam(param, timesecs, timestepsec);
	param     = Q.Param;
	timesecs  = Q.Time;
	mySDate   = new Date(timesecs[0] * 1000);
	myEDate   = new Date(timesecs[timesecs.length-1] * 1000);
	//console.log(param,timesecs,mySDate,myEDate);


	// Create the highcharts Reference Q object
	$(function () {
		$('#highchart-refhydro').highcharts({
			title: {
				text: '<?php echo 'USGS Station: ' . $station; ?>',
			},
			xAxis: {
				type: 'datetime',
				labels: {
					formatter: function () {
						return Highcharts.dateFormat('%H:%M <br/> %b %d <br/> %Y', this.value);
					},
				
				align: 'center'
				},
			},
			yAxis: {
				title: {
					text: 'Discharge'
				},
				type: 'logarithmic',
				minorTickInterval: 0.1,
			},
			tooltip: {
				shared: true
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				area: {
					color: '#0B0B61',  // midnight blue
					lineWidth: 1.5,
					marker: {
						enabled: false
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1.5
						}
					},
					threshold: null
					
				},
			},

			series: [{
				type: 'area',
				name: 'Q, in cfs',
				id: 'series-1',
				pointInterval: 1800 * 1000, // 30 minutes (1800s as milliseconds)
				pointStart: timesecs[0] * 1000,
				data: param,
				fillOpacity: 0,
			},{
				type: 'area',
				name: 'time control',
				pointInterval: 1800 * 1000, // 30 minutes (1800s as milliseconds)
				pointStart: timesecs[0] * 1000,
				data: [null],
				animations: false,
				linewidth: 0,
				fillColor: {
					linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				},
				}]
			});
		});
	</script>
</DIV>
</FIELDSET>
<FIELDSET> <LEGEND>Water Surface Profiles</LEGEND> 
<DIV ID="highchart-wseprofile">
<SCRIPT TYPE="text/javascript">
	// Interpolate a consistent timestep (30 minutes) for each 
	// WSE Profile Station.
	// For now, this is just for CAWS...I need to figure out how to 
	// make this generic to dynamic inputs
	
	// First, create JS variables of the PHP outputs
	var timestepsec = 1800;
	var S05536998 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536998) . "]";?>, <?php  echo "[" . implode(',', $rtime05536998) . "]";?>, timestepsec);
	var S05536890 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536890) . "]";?>, <?php  echo "[" . implode(',', $rtime05536890) . "]";?>, timestepsec);
	var S05536118 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536118) . "]";?>, <?php  echo "[" . implode(',', $rtime05536118) . "]";?>, timestepsec);
	var S05536123 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536123) . "]";?>, <?php  echo "[" . implode(',', $rtime05536123) . "]";?>, timestepsec);
	var S05536121 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536121) . "]";?>, <?php  echo "[" . implode(',', $rtime05536121) . "]";?>, timestepsec);
	var S04087440 = interpolateparam(<?php  echo "[" . implode(',', $rstage04087440) . "]";?>, <?php  echo "[" . implode(',', $rtime04087440) . "]";?>, timestepsec);
	var rmile = <?php  echo "[" . implode(',', $rmile) . "]";?>;
	
	// Create a point constructor to make the Highcharts data string
	function Point(x, y) {
	  this.x = x;
	  this.y = y;
	}
	
	// Loop through each station and output the point into an array
	var a = [];
	for (i = 0; i < 1; i++) {
		a.push(new Point(rmile[0],S05536998.Param[0]));
		a.push(new Point(rmile[1],S05536890.Param[0]));
		a.push(new Point(rmile[2],S05536118.Param[0]));
		a.push(new Point(rmile[3],S05536123.Param[0]));
		a.push(new Point(rmile[4],S05536121.Param[0]));
		a.push(new Point(rmile[5],S04087440.Param[0]));
	}
	
	
	
	/*var Q = interpolateparam(param, timesecs, timestepsec);
	param     = Q.Param;
	timesecs  = Q.Time;
	mySDate   = new Date(timesecs[0] * 1000);
	myEDate   = new Date(timesecs[timesecs.length-1] * 1000);
	*/
	
	
	// Create the highcharts WSE object
	$(function () {
		$('#highchart-wseprofile').highcharts({
			title: {
				text: Date(timesecs[0] * 1000),
			},
			xAxis: {
				title: {
					text: 'River Mile',
				},
			},
			yAxis: {
				title: {
					text: 'Elevation, CCD',
				},
				min: - 10,
				max: 5,
			},
			tooltip: {
				shared: true
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				line: {
					color: '#0B0B61',  // midnight blue
					lineWidth: 1.5,
					marker: {
						enabled: true
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1.5
						}
					},
					threshold: null
				}
			},
			series: [{
				name: 'WSE',
				data: a,
			}]
		});
	});
	
   </SCRIPT>              
  
</DIV>
</FIELDSET>
<FIELDSET> <LEGEND>Plot Animation</LEGEND> 
<DIV ID="highchart-controls">
	<script>
	   function chartAnimation() {
		var t=setInterval(updateChart,100);
			var ii = 0;
			function updateChart() {
				// stuff you want to do every second
				var chart1 = $('#highchart-refhydro').highcharts();
				var c1time = chart1.get('series-1').yData[ii];
				//console.log(c1time);
				var chart2 = $('#highchart-wseprofile').highcharts();
				var newData = [];
				newData.push(new Point(rmile[0],S05536998.Param[ii]));
				newData.push(new Point(rmile[1],S05536890.Param[ii]));
				newData.push(new Point(rmile[2],S05536118.Param[ii]));
				newData.push(new Point(rmile[3],S05536123.Param[ii]));
				newData.push(new Point(rmile[4],S05536121.Param[ii]));
				newData.push(new Point(rmile[5],S04087440.Param[ii]));
				chart1.series[1].addPoint(c1time);
				$(chart2.series[0].data).each( function (i, point) {
					 point.update(newData[i], false);
				});
				var timestamp = new Date(timesecs[ii] * 1000);
				//console.log(timestamp.toUTCString);
				chart2.setTitle({ text: timestamp.toUTCString });
				chart2.redraw();
				ii += 1;
				//console.log(timesecs[ii]);
				// Conditional break
				if (ii == S05536998.Param.length) { //S05536998.Param.length
				clearInterval(t);
				}
			}
	};
	</script>
	<button TYPE="submit" VALUE="Submit" id="playChart" onclick="chartAnimation();return false">Play From Beginning</button>
	
   
</DIV>
</FIELDSET>
</form>