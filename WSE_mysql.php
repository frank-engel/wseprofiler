 
<!doctype html>
<head>
	<title>Testing WSE Profiler with mySQL Support</title>
	<!-- Include jQuery for themes and AJAX control -->
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>
	<script src="http://jquery.bassistance.de/validate/additional-methods.js"></script>
	<SCRIPT SRC="//code.jquery.com/ui/1.10.4/jquery-ui.js"></SCRIPT> 
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.15/jquery-ui.min.js"></script>
	<script src="./js/highcharts.js"></script>
	<script src="./js/modules/exporting.js"></script>
	<script type="text/javascript" src="createInterpolant.js"></script>
	<!-- drop down calendars -->
	<link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" REL="stylesheet">
	<script>
	$(function() {
		$( "#startdatepicker" ).datepicker();
	});
	$(function() {
		$( "#enddatepicker" ).datepicker();
	});
	
	
	
	
	</script> 

	
	

</head>
<body>

<DIV ID="tabs-1"> 
	<FORM ACTION="#" METHOD="GET" Name="InputForm" id="InputForm">
	<FIELDSET> <LEGEND>Input Parameters</LEGEND> 
	<UL CLASS="form-fields">
	<!--<LI><LABEL FOR="reference_hydrograph">Reference Station</LABEL>
	<INPUT CLASS="text-input" ID="reference_hydrograph"
		  NAME="reference_hydrograph" TYPE="text" value="05536890"> </LI> -->
		  <?php
    if (empty($_GET)) {
    // no data passed by get, load last 7 days
	$startdate = date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d'))));
	$enddate   = date('Y-m-d');
}
else
{
$startdate = htmlspecialchars($_GET["start_date"]);
$enddate   = htmlspecialchars($_GET["end_date"]);
}?>
	<LI><LABEL FOR="start_date">Start Date (yyyy-mm-dd)</LABEL> 
	<INPUT CLASS="text-input" ID="startdatepicker"
		  NAME="start_date" TYPE="text" value=
		  <?php 
		  if (empty($_GET)) {
		   echo date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d')))); 
		   }
		   else { 
		   echo htmlspecialchars($_GET["start_date"]);
		   }?>> 
		  </LI>
	<LI><LABEL FOR="end_date">End Date (yyyy-mm-dd)</LABEL> 
	<INPUT CLASS="text-input" ID="enddatepicker" NAME="end_date"
		  TYPE="text" value=
		  <?php 
		  if (empty($_GET)) {
		   echo date('Y-m-d'); 
		   }
		   else { 
		   echo htmlspecialchars($_GET["end_date"]);
		   }?>>
		   </LI>
	<!--<LI>

	<P CLASS="form-help">Input the USGS station numbers and
	river mile information following the format example (csv coordinate pairs). List
	stations in ascending order by river mile.</P> 
	<LABEL FOR="profile_stations">Stations</LABEL> 
	<TEXTAREA CLASS="text-input" ID="profile_stations" NAME="profile_stations" TYPE="text" COLS="20" Rows="5">
05536998,291.1,
05536890,303.0,
05536118,325.9,
05536123,326.4,
05536121,326.7,
04087440,326.8
	</TEXTAREA></LI> -->
	<button TYPE="submit" VALUE="Submit" ID="submitbtn">Submit</button>
	<button TYPE="reset" VALUE="Reset" ID="resetbtn">Clear Form</button>
	</UL>
	</FIELDSET>
	</form>
</div> <!-- tabs-1-->

<div id="php_computations">
<?php

$mysqlparams = array(
	"host" => "localhost",
	"user" => "webapp",
	"pw"   => "rraipviedr",
	"database" => "wseprofiler",
	"table"    => "gh05536123",
	"fields"   => "site_no, datetime, gh"
); 
if (empty($_GET)) {
    // no data passed by get, load last 7 days
	$startdate = date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d'))));
	$enddate   = date('Y-m-d');
}
else
{
$startdate = htmlspecialchars($_GET["start_date"]);
$enddate   = htmlspecialchars($_GET["end_date"]);
}
include "GetData.php";
$out = array();
$tables = array("gh05536998","gh05536890","gh05536123","gh05536121","gh05536118", "gh04087440",
"q05536890");

// Query the database and return assoc arrays for all gages in daterange
foreach ($tables as $value) 
{
	switch ($value) 
	{
		case "q05536890":
			$mysqlparams["table"] = $value;
			$mysqlparams["fields"] = "site_no, datetime, q";
			$out[$value] = GetData($mysqlparams,$startdate,$enddate);

			/* echo "<br><pre>";
			print_r($out[$value][0]);
			$ii = $ii + 1;
			print_r($ii);
			echo "</pre>"; */
			break;
		default:
			$mysqlparams["table"] = $value;
			$mysqlparams["fields"] = "site_no, datetime, gh";
			$out[$value] = GetData($mysqlparams,$startdate,$enddate);

			/* echo "<br><pre>";
			print_r($out[$value][0]);
			$ii = $ii + 1;
			print_r($ii);
			echo "</pre>"; */
			break;
	}
}

// Build the reference hydrograph
$datetime = array();
$q = array();
$qdata = array();
foreach ($out["q05536890"] as $key => $value)
{
	$datetime[] = strtotime($out["q05536890"][$key]["datetime"])*1000; // js compliant
	$q[]        = (float)$out["q05536890"][$key]["q"];
	$qdata[]      = array(
					'x' => strtotime($out["q05536890"][$key]["datetime"])*1000,
					'y' => (float)$out["q05536890"][$key]["q"]
					);
}


// Build the WSE arrays
//$time_pre = microtime(true);
$x = array(291.1,303.0,325.9,326.4,326.7,326.8);
$y = array();
$data = array();
$ii = 0;
foreach (array_slice($tables,0,6) as $value1) // loop through gh tables
{
	foreach ($out[$value1] as $key2 => $value2)
	{
		$dt[$key2] = strtotime($out[$value1][$key2]["datetime"])*1000; // js compliant
		$dy[$key2] = $out[$value1][$key2]["gh"];
		//echo $value1 . "   " . $out[$value1][$key2]["gh"];
	}

	//$data[] = array('x' => (float)$x[$ii], 'y'=>(float)$out[$value1][0]["gh"]);
	//$data["y"][] = $out[$value1][0]["gh"]; 
	$data[$value1]["time"] = $dt;
	$data[$value1]["wse"] = $dy;
	foreach($dy as $k1 => $v1) { 
		if ($dy[$k1] == "0") {
			$dy[$k1] = "-99";
		}
	}
	$data[$value1]["dist"] = array_fill(0,count($dy),$x[$ii]);
	$ii = $ii + 1;
	$y[] = (float)$out[$value1][0]["gh"]; 
}
?>

</div>

<div id="container_qref"></div>
<div id="container_wse"></div>
<script>
// Interpolate a consistent timestep (30 minutes)
// This is the reference hydrograph
	var param = <?php echo json_encode(array_column($qdata,'y'));?>;
	var timesecs = <?php echo json_encode(array_column($qdata,'x')); ?>;
	var timestepsec = 1800 * 1000; // 30 minutes expressed as milliseconds
	var interpolateparam = function(param, timesecs, timestepsec) {
		var eidx = timesecs.length -1; // zero based indexing
		
		var f = createInterpolant(timesecs, param);
		var resultParam = []; resultTime = [];
		for (var x = timesecs[0]; x <= timesecs[eidx]; x+=timestepsec) {
			resultParam.push(f(x));
			resultTime.push(x);
		};
		return {
				Param: resultParam,
				Time: resultTime
			};
	};
	var Q = interpolateparam(param, timesecs, timestepsec);
	param     = Q.Param;
	timesecs  = Q.Time;
	mySDate   = new Date(timesecs[0]);
	myEDate   = new Date(timesecs[timesecs.length-1]);
	
// Highcharts object containing reference hydrograph
var qref_options = {
	chart: {
            renderTo: 'container_qref',
			animation: false
			},
			title: {
				text: 'Ref Q',
			},
			xAxis: {
				type: 'datetime',
				labels: {
					formatter: function () {
						return Highcharts.dateFormat('%H:%M <br/> %b %d <br/> %Y', this.value);
					},
				
				align: 'center'
				},
			},
			yAxis: {
				title: {
					text: 'Discharge'
				},
				type: 'logarithmic',
				minorTickInterval: 0.1,
			},
			tooltip: {
				shared: true
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				area: {
					color: '#0B0B61',  // midnight blue
					//lineWidth: 1.5,
					marker: {
						enabled: false
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1.5
						}
					},
					threshold: null,
					animation: false
				},
			},

			series: [{
				type: 'area',
				name: 'Q, in cfs',
				id: 'series-1',
				pointInterval: 1800 * 1000, // 30 minutes (1800s as milliseconds)
				pointStart: timesecs[1],
				//pointStart: Date.UTC(2013, 3, 15),
				data: param,
				fillOpacity: 0,
			},{
				type: 'area',
				name: 'time control',
				id: 'series-2',
				pointInterval: 1800 * 1000, // 30 minutes (1800s as milliseconds)
				pointStart: timesecs[0],
				//pointStart: Date.UTC(2013, 3, 15),
				data: [null],
				animations: false,
				linewidth: 0,
				fillColor: {
					linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				},
				}]
			};
			
//$(function () {
//		$('#container_qref').highcharts(qref_options);
//});
var chart1 = new Highcharts.Chart(qref_options);

</script>
		
<script type="text/javascript">
// Interpolate a consistent timestep (30 minutes)
// This is the WSE
// First, create JS variables of the PHP outputs
	var timestepsec = 1800*1000;
	var S05536998 = interpolateparam(<?php  echo json_encode($data['gh05536998']['wse']);?>, <?php  echo json_encode($data['gh05536998']['time']);?>, timestepsec);
	var S05536890 = interpolateparam(<?php  echo json_encode($data['gh05536890']['wse']);?>, <?php  echo json_encode($data['gh05536890']['time']);?>, timestepsec);
	var S05536123 = interpolateparam(<?php  echo json_encode($data['gh05536123']['wse']);?>, <?php  echo json_encode($data['gh05536123']['time']);?>, timestepsec);
	var S05536121 = interpolateparam(<?php  echo json_encode($data['gh05536121']['wse']);?>, <?php  echo json_encode($data['gh05536121']['time']);?>, timestepsec);
	var S05536118 = interpolateparam(<?php  echo json_encode($data['gh05536118']['wse']);?>, <?php  echo json_encode($data['gh05536118']['time']);?>, timestepsec);
	var S04087440 = interpolateparam(<?php  echo json_encode($data['gh04087440']['wse']);?>, <?php  echo json_encode($data['gh04087440']['time']);?>, timestepsec);
	var rmile = <?php  echo json_encode($x);?>;
	
	// Create a point constructor to make the Highcharts data string
	function Point(x, y) {
	  this.x = x;
	  this.y = y;
	}
	
	// Loop through each station and output the point into an array
	var a = [];
	for (i = 0; i < 1; i++) {
		a.push(new Point(rmile[0],S05536998.Param[0]));
		a.push(new Point(rmile[1],S05536890.Param[0]));
		a.push(new Point(rmile[2],S05536118.Param[0]));
		a.push(new Point(rmile[3],S05536123.Param[0]));
		a.push(new Point(rmile[4],S05536121.Param[0]));
		a.push(new Point(rmile[5],S04087440.Param[0]));
	}
	

// Create the highcharts WSE object
	$(function () {
		$('#container_wse').highcharts({
			chart: {
            animation: false
			},
			title: {
				text: 'Stage',
			},
			xAxis: {
				title: {
					text: 'River Mile',
				},
			},
			yAxis: {
				title: {
					text: 'Elevation, CCD',
				},
				min: - 10,
				max: 5,
			},
			tooltip: {
				shared: true
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				line: {
					color: '#0B0B61',  // midnight blue
					lineWidth: 1.5,
					marker: {
						enabled: true
					},
					shadow: false,
					states: {
						hover: {
							lineWidth: 1.5
						}
					},
					threshold: null,
					animation: false
				}
			},
			series: [{
				name: 'WSE',
				id: 'series-1',
				data: a,
				fillColor: '#0000CC',
			}]
		});
	});

</script>
<div id="playback_controls">
<script>
   function chartAnimation() {
		var t=setInterval(updateChart,100);
		var ii = 0;
		function updateChart() {
			// stuff you want to do every second
			var chart1 = $('#container_qref').highcharts();
			var c1time = chart1.get('series-1').yData[ii];
			//var c1orig = chart1.get('series-1').data;
			var chart2 = $('#container_wse').highcharts();
			var newData = [];
			newData.push(new Point(rmile[0],S05536998.Param[ii]));
			newData.push(new Point(rmile[1],S05536890.Param[ii]));
			newData.push(new Point(rmile[2],S05536118.Param[ii]));
			newData.push(new Point(rmile[3],S05536123.Param[ii]));
			newData.push(new Point(rmile[4],S05536121.Param[ii]));
			newData.push(new Point(rmile[5],S04087440.Param[ii]));
			chart1.series[1].addPoint(c1time);
			$(chart2.series[0].data).each( function (i, point) {
				 point.update(newData[i], false);
			});
			var timestamp = new Date(timesecs[ii]);
			chart2.setTitle({ text: timestamp.toUTCString() });
			chart2.redraw();
			ii += 1;
			//console.log(timesecs[ii]);
			// Conditional break
			if (ii == S05536998.Param.length) { //S05536998.Param.length
			clearInterval(t);
			//chart1.series[0].setData(c1orig);
			}
		}
};
</script>
	<button TYPE="submit" VALUE="Submit" id="playChart" >Play From Beginning</button>
</div>
<script>
$('#playChart').click(function(){
	// reset the chart
	chart1.destroy();
    chart1 = new Highcharts.Chart(qref_options);
	chartAnimation();
});
</script>
</body>