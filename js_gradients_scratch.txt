// Keep the gradient fill stuff for animation

fillColor: {
	linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
	stops: [
		[0, Highcharts.getOptions().colors[0]],
		[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
	]
},