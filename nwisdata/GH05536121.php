<?php
// Query NWIS Web for data, append new data to mysql table
// Gage: 05536121
// Parameter: Gage Height (cb_00065)

// NWIS parameters
$subfolder = "";
$station="05536121";
$localhost = ini_get("mysql.default_host");
$mysql_user = 'webapp';
$mysql_password = 'rraipviedr';
$mysql_database = 'wseprofiler';
$mysql_table = "GH" . $station;

// Pull latest date in the mysql database
$conn = mysql_connect($localhost, $mysql_user, $mysql_password);
if (!$conn) {
	die('Could not connect: ' . mysql_error());
}
mysql_select_db( $mysql_database, $conn );
$sql = "select site_no , datetime from " . $mysql_table . 
" where datetime in (select max(datetime) from " . $mysql_table . ");";
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_assoc($result);
$latest_time = explode(" ", $row["datetime"]);
//print_r("Date: " . $latest_time[0] . " Time: " . $latest_time[1]);
mysql_free_result($result);
mysql_close($conn);

// Setup NWIS query
$startdate= $latest_time[0]; //date("Y-m-d"); // grab today's new data
$enddate=date("Y-m-d");
$parameter = "cb_00065";  // stage

// Read some RDB format data using a URL query to NWIS Web (2 parameter)
$url = "http://nwis.waterdata.usgs.gov/usa/nwis/uv?" . $parameter ."=on&format=rdb&period=&begin_date=" . 
$startdate . "&end_date=" . $enddate . "&site_no=" . $station ."&referred_module=sw";
$content=file_get_contents($url);

// Strip the input of the header, then append result to the existing data
// file stored on the server
$rows = explode("\n", $content);
$maxHeaderSize = 100; // First parse of the RDB to find header offset
for ($offset = 0; $offset <= $maxHeaderSize; $offset++) {
    // Take header row by row. Stop when the first character 
	// is no longer "#"
	$irow = trim($rows[$offset]);
	$irow = preg_replace('/\s+/',' ',$irow);
	$icol = explode(" ", $irow);
	if (!in_array("#",$icol)) {
		$offset = $offset + 2; // next row starts the data
		break; 
	} 
}

// Parse each row
// Delete last element if empty (happens if there is a LF/CR character
$data = array_slice($rows,$offset);
$i = 0;
foreach($data as $key => $value){
    $cols[$i] = explode("\t", $value);
    $i++;
}
if(empty($cols[count($cols)-1][0])) {
    unset($cols[count($cols)-1]);
}

// Build associative array
$keys = array('agency_cd','site_no','datetime','tz_cd','GH','GH_cd');
foreach ($cols as $ii => $iival) {
	$cols[$ii] = array_combine($keys,$cols[$ii]);
	unset($cols[$ii]['GH_cd']);
}

// Get data newer than the last record in the mysql date
function searchfordate($inarray, $field, $needle)
{
   foreach($inarray as $key => $inarray)
   {
      if ( strtotime($inarray[$field]) == $needle )
         return $key;
   }
   return false;
}
$searchfor =  $latest_time[0] . " " . $latest_time[1];
$searchfor = strtotime($searchfor);
$key = searchfordate($cols, "datetime", $searchfor);
$update = array_slice($cols,$key+1,count($cols));


// Connect to mysql and send the update
$conn = mysql_connect($localhost, $mysql_user, $mysql_password);
if (!$conn) {
	die('Could not connect: ' . mysql_error());
}
mysql_select_db( $mysql_database, $conn );
$sql  = "INSERT INTO " . $mysql_table;
$sql .= " (`".implode("`, `", array_keys($update[0]))."`) VALUES"; 
foreach ($update as $array) {
	$sql .= " ('".implode("', '", $array)."'),";
}
$sql = rtrim($sql, ","); // remove last comma
$result = mysql_query($sql) or die(mysql_error());
mysql_close($conn);

// Print a copy of the SQL query
?>
<pre>
<?php
print_r($sql);
?>
</pre>
<?php
?>