<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html> 
<head>
   <title>Water Surface Profile Webapp Development</TITLE> 
   <?php
	//error handler function
	function customError($errno, $errstr){
	  echo "<b>Error:</b> [$errno] $errstr";
	}

	//set error handler
	set_error_handler("customError");
	?>

<!-- Include jQuery for themes and AJAX control -->
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
   <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script> 

<!-- Load a style sheet -->
   <link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
   <script src="//code.jquery.com/jquery-1.9.1.js"></script> 
   <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script> 
   <link href="style.css" rel="stylesheet">
   
   <script>
   // Create the UI Tabs
		$(function(){
			$( "#tabs" ).tabs();

		});
   </script> 

<!-- This script creates drop down calendars -->
   <link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"
         REL="stylesheet">
   <script>
	  $(function(){
		$( "#startdatepicker" ).datepicker();
	  });
	  $(function(){
		$( "#enddatepicker" ).datepicker();
	  });
   </script> 

<!-- Load Highcharts -->
   <script src="./js/highcharts.js"></script> 
   <script src="./js/modules/exporting.js"></script>

<!-- User scripts -->
   <script src="resampleparam.js" type="text/javascript"> </script>

  
</head>


<body  onload="document.Inputform.reset();">
<!-- Loading animation -->
<div id="loading">
  <span class="helper"></span><img id="loading-image" src="ajax-loader.gif" alt="Loading..." /><p>Processing, please wait...</p>
</div>
<script type="text/javascript">
    $(function(){
        $("#submitbtn").click(function(){
            $("#loading").fadeIn();
			var selected = $("#tabs").tabs("option", "active");
			$("#tabs").tabs("option", "active", selected + 1);
        });
    });
</script>

<div id="content"> 
   <h1>Water Surface Profile Webapp (development alpha)</h1> 
   
<!-- TAB CONTROL -->
   <div id="tabs"> 
      <ul>
         <li><a href="#tabs-1">Setup Application</A></li> 
         <li><a href="#tabs-2">Plotting</A></li> 
         <li><a href="#tabs-3">Reserved</A></li> 
      </ul>
	  
<!-- SETUP APPLICATIONS TAB-->
      <div id="tabs-1"> 
         <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"
               method="POST" name="Inputform">
         <fieldset> <legend>Input Parameters</legend> 
            <ul class="form-fields">
               <li><label for="reference-hydrograph">Reference Station</LABEL>
               
               <input class="text-input" id="reference-hydrograph"
                      name="reference-hydrograph" type="text"> </li>
               <li><label for="start-date">Start Date</LABEL> 
               <input class="text-input" id="startdatepicker"
                      name="start-date" type="text"> </li>
               <li><label for="end-date">End Date</LABEL> 
               <input class="text-input" id="enddatepicker" name="end-date"
                      type="text"> </li>
               <li>

               <p class="form-help">Input the USGS station numbers and
               river mile information following the format example (csv coordinate pairs). List
               stations in ascending order by river mile.</P> 
               <label for="profile-stations">Stations</LABEL> 
<textarea class="text-input" id="profile-stations" name="profile-stations" type="text" COLS="20" Rows="5">
05536998,291.1,
05536890,303.0,
05536118,325.9,
05536123,326.4,
05536121,326.7,
04087440,326.8
</textarea></li>
               <button type="submit" VALUE="Submit" id="submitbtn">Submit</button>
			   <button type="reset" VALUE="Reset" id="resetbtn">Clear form</button>
            </ul>
         </fieldset>
	<!-- Begin scripts to Process form inputs-->
    <!--begin test here-->
	<?php
	// Validate the form data and reformat date string
	function test_input($formdata)
	{
	  $formdata=trim($formdata);
	  $formdata=stripslashes($formdata);
	  $formdata=htmlspecialchars($formdata);
	  return $formdata;
	}

	// define variables and set to empty values
	$station=$startdate=$enddate=$profilestations="";//shouldn't have to do this in php

	if ($_SERVER["REQUEST_METHOD"]=="POST")
	{
	  $station=test_input($_POST["reference-hydrograph"]);
	  $startdate=test_input($_POST["start-date"]);
	  $enddate=test_input($_POST["end-date"]);
	  $profilestations=test_input($_POST["profile-stations"]);
	  
	};
	// Reformat the dates
	$startdate=str_replace("/","-",$startdate);
	$enddate=str_replace("/","-",$enddate);

	// Query NWIS for Reference Station Discharge
	$parameter = "cb_00060";  // discharge
	include 'nwisrequest.php';
	list($refQtime, $discharge) = read_nwis($station,$startdate,$enddate,$parameter);
	
	// Parse the input WSE Stations and query NWIS
	$profilestations = explode(',', $profilestations);
	$size = count($profilestations);
	$rsta = array(); $rmile = array();
	for ($i = 0; $i < $size; $i += 2) {
	  $rsta[]  = trim($profilestations[$i]);
	  $rmile[] = trim($profilestations[$i+1]);
	}

	$parameter = "cb_00065";  // stage
	$size   = count($rsta);
	$rtime  = array();
	$rstage = array();

	for($i=0; $i < $size; $i++){
		list($rtime[$rsta[$i]],$rstage[$rsta[$i]]) = read_nwis($rsta[$i],$startdate,$enddate,$parameter);
	}
	
	function toFloats($array) {
    return array_map('floatval', $array);
	}
	$rtime  = array_map('toFloats', $rtime);
	$rstage = array_map('toFloats', $rstage);
		
	// Create flat arrays containing the data for each WSE station
    for ($i = 0; $i < $size; $i++){
		${"rtime".$rsta[$i]}  = array_values($rtime[$rsta[$i]]);
		${"rstage".$rsta[$i]} = array_values($rstage[$rsta[$i]]);
	}
	
	// TEMP: Convert Lemont to CCD
	$ii = count($rstage05536890);
	for ($i = 0; $i < $ii; $i++){
		$rstage05536890[$i] = ((552.049 + $rstage05536890[$i]) - 579.48);
	}
			
	?> 
	<!--end test here-->
	</div> <!-- END SETUP APPLICATION TAB -->

<!-- PLOTTING TAB -->
      <div id="tabs-2"> 
         <form>
         <fieldset> <legend>Reference Hydrograph</legend> 
            <div id="highchart-refhydro"> 
				<script type="text/javascript" src="createInterpolant.js"></script>
				<script>
				// Interpolate a consistent timestep (30 minutes)
				var param = <?php  echo "[" . implode(',', $discharge) . "]";?>;
				var timesecs = <?php  echo "[" . implode(',', $refQtime) . "]"; ?>;
				var timestepsec = 1800;
				var interpolateparam = function(param, timesecs, timestepsec) {
					var eidx = timesecs.length -1; // zero based indexing
					
					var f = createInterpolant(timesecs, param);
					var resultParam = []; resultTime = [];
					for (var x = timesecs[0]; x <= timesecs[eidx]; x+=timestepsec) {
						resultParam.push(f(x));
						resultTime.push(x);
					};
					return {
						Param: resultParam,
						Time: resultTime
					};
				};
				var Q = interpolateparam(param, timesecs, timestepsec);
				param     = Q.Param;
				timesecs  = Q.Time;
				mySDate   = new Date(timesecs[0] * 1000);
				myEDate   = new Date(timesecs[timesecs.length-1] * 1000);
			    //console.log(param,timesecs,mySDate,myEDate);
				
               
			   // Create the highcharts Reference Q object
			   $(function () {
					$('#highchart-refhydro').highcharts({
						title: {
							text: '<?php echo 'USGS Station: ' . $station; ?>',
						},
						xAxis: {
							type: 'datetime',
							labels: {
								formatter: function () {
									return Highcharts.dateformat('%H:%M <br/> %b %d <br/> %Y', this.value);
								},
							
							align: 'center'
							},
						},
						yAxis: {
							title: {
								text: 'Discharge'
							},
							type: 'logarithmic',
							minorTickInterval: 0.1,
						},
						tooltip: {
							shared: true
						},
						legend: {
							enabled: false
						},
						plotOptions: {
							line: {
								color: '#0B0B61',  // midnight blue
								lineWidth: 1.5,
								marker: {
									enabled: false
								},
								shadow: false,
								states: {
									hover: {
										lineWidth: 1.5
									}
								},
								threshold: null
							}
						},
				
						series: [{
							type: 'line',
							name: 'Q, in cfs',
							pointInterval: 1800 * 1000, // 30 minutes (1800s as milliseconds)
							pointStart: timesecs[0] * 1000,
							data: param,
									}]
								});
							});
				</script>
			
            </div>
         </fieldset>
         <fieldset> <legend>Water Surface Profiles</legend> 
            <div id="highchart-wseprofile"> 
				<script type="text/javascript">
				// Interpolate a consistent timestep (30 minutes) for each 
				// WSE Profile Station.
				// For now, this is just for CAWS...I need to figure out how to 
				// make this generic to dynamic inputs
				
				// First, create JS variables of the PHP outputs
				var timestepsec = 1800;
				var S05536998 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536998) . "]";?>, <?php  echo "[" . implode(',', $rtime05536998) . "]";?>, timestepsec);
				var S05536890 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536890) . "]";?>, <?php  echo "[" . implode(',', $rtime05536890) . "]";?>, timestepsec);
				var S05536118 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536118) . "]";?>, <?php  echo "[" . implode(',', $rtime05536118) . "]";?>, timestepsec);
				var S05536123 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536123) . "]";?>, <?php  echo "[" . implode(',', $rtime05536123) . "]";?>, timestepsec);
				var S05536121 = interpolateparam(<?php  echo "[" . implode(',', $rstage05536121) . "]";?>, <?php  echo "[" . implode(',', $rtime05536121) . "]";?>, timestepsec);
				var S04087440 = interpolateparam(<?php  echo "[" . implode(',', $rstage04087440) . "]";?>, <?php  echo "[" . implode(',', $rtime04087440) . "]";?>, timestepsec);
				var rmile = <?php  echo "[" . implode(',', $rmile) . "]";?>;
				
				// Create a point constructor to make the Highcharts data string
				function Point(x, y) {
				  this.x = x;
				  this.y = y;
				}
				
				// Loop through each station and output the point into an array
				var a = [];
				for (i = 0; i < 1; i++) {
					a.push(new Point(rmile[0],S05536998.Param[0]));
					a.push(new Point(rmile[1],S05536890.Param[0]));
					a.push(new Point(rmile[2],S05536118.Param[0]));
					a.push(new Point(rmile[3],S05536123.Param[0]));
					a.push(new Point(rmile[4],S05536121.Param[0]));
					a.push(new Point(rmile[5],S04087440.Param[0]));
				}
				
				
				
				/*var Q = interpolateparam(param, timesecs, timestepsec);
				param     = Q.Param;
				timesecs  = Q.Time;
				mySDate   = new Date(timesecs[0] * 1000);
				myEDate   = new Date(timesecs[timesecs.length-1] * 1000);
				*/
				
				
				// Create the highcharts WSE object
				$(function () {
					$('#highchart-wseprofile').highcharts({
						title: {
							text: Date(timesecs[0] * 1000),
						},
						xAxis: {
							title: {
								text: 'River Mile',
							},
						},
						yAxis: {
							title: {
								text: 'Elevation, CCD',
							},
							min: - 10,
							max: 5,
						},
						tooltip: {
							shared: true
						},
						legend: {
							enabled: false
						},
						plotOptions: {
							line: {
								color: '#0B0B61',  // midnight blue
								lineWidth: 1.5,
								marker: {
									enabled: true
								},
								shadow: false,
								states: {
									hover: {
										lineWidth: 1.5
									}
								},
								threshold: null
							}
						},
						series: [{
							name: 'WSE',
							data: a,
						}]
					});
				});
				
			   </script>              
			  
            </div>
         </fieldset>
         <fieldset> <legend>Plot Animation</legend> 
            <div id="highchart-controls"> 
               <button id="playChart">Play From Beginning</button>
				
			   <script>	
			   // Loop through each station and output the point into an array
			   //console.log(S05536998.Param.length);
				
				
				
				
			   
			   //$('#playChart').click(function() {
					var t=setInterval(updateChart,100);
						var ii = 0;
						function updateChart() {
							// stuff you want to do every second
							var chart2 = $('#highchart-wseprofile').highcharts();
							var newData = [];
							newData.push(new Point(rmile[0],S05536998.Param[ii]));
							newData.push(new Point(rmile[1],S05536890.Param[ii]));
							newData.push(new Point(rmile[2],S05536118.Param[ii]));
							newData.push(new Point(rmile[3],S05536123.Param[ii]));
							newData.push(new Point(rmile[4],S05536121.Param[ii]));
							newData.push(new Point(rmile[5],S04087440.Param[ii]));
							$(chart2.series[0].data).each( function (i, point) {
								 point.update(newData[i], false);
							});
							chart2.setTitle({ text: Date(timesecs[ii] * 1000) });
							chart2.redraw();
							ii += 1;
							console.log(timesecs[ii]);
							// Conditional break
							if (ii == S05536998.Param.length) { //S05536998.Param.length
							clearInterval(t);
							}
						}
				//});
				</script>
            </div>
         </fieldset>
      </div> <!-- END PLOTTING TAB -->

<!-- RESERVED TAB -->
      <div id="tabs-3"> 
		<?php echo '
        <H2>The form inputs:</H2>'; echo '
        <H3>Reference Station:</H3>'; echo "
		<PRE>";
		 
		print_r($station); //explore results
		echo "</PRE>"; echo '
        <H3>Start date (formatted with dashes):</H3>'; echo "
		<PRE>";
		print_r($startdate); //explore results
		echo "</PRE>"; echo '
        <H3>End date (formatted with dashes):</H3>'; echo "
		<PRE>";
		print_r($enddate); //explore results
		echo "</PRE>"; echo '
        <H3>Stations with river mile:</H3>'; echo "
		<PRE>";
		print_r($profilestations); //explore results
		echo "</PRE>"; ?> 
      </div> <!-- END RESERVED TAB -->
   </div> <!-- END TAB CONTROL -->
</div> <!-- END CONTENT -->




</body>
</html> 
