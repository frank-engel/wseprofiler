<?php
// Fetch all Gage Heights for specified input mysql table
function GetData($mysqlparams,$startdate,$enddate)
/* $mysqlparams = array(
	"host" => "localhost",
	"user" => "webapp",
	"pw"   => "rraipviedr",
	"database" => "wseprofiler"
	"table"    => "gh05536123"
	"fields"   => "site_no, datetime, gh"
); 

$startdate = '2013-04-15';
$enddate   = '2013-04-21';
*/
{
$link = mysqli_connect(
	$mysqlparams["host"], $mysqlparams["user"], $mysqlparams["pw"], $mysqlparams["database"]);

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$mysql_table = $mysqlparams["table"];
$query = "SELECT " . $mysqlparams["fields"] . " FROM " . $mysqlparams["table"] . " where datetime >= '" . $startdate ."' AND datetime <= '" . $enddate . "';";

if ($result = mysqli_query($link, $query)) {

	$data = array();
	/* fetch associative array */
    while ($row = mysqli_fetch_assoc($result)) {
    $data[] = $row;
	}
	
    /* free result set */
    mysqli_free_result($result);
}

/* close connection */
mysqli_close($link);
return $data;
} //end function
?>