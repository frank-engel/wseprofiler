

<!-- Validate the form data and reformat date string-->
<?php
function test_input($formdata)
{
  $formdata=trim($formdata);
  $formdata=stripslashes($formdata);
  $formdata=htmlspecialchars($formdata);
  return $formdata;
}

// define variables and set to empty values
$station=$startdate=$enddate=$profilestations="";

if ($_SERVER["REQUEST_METHOD"]=="POST")
{
  $station=test_input($_POST["reference-hydrograph"]);
  $startdate=test_input($_POST["start-date"]);
  $enddate=test_input($_POST["end-date"]);
  $profilestations=test_input($_POST["profile-stations"]);
  
};
// Reformat the dates
$startdate = str_replace("/","-");
$enddate   = str_replace("/","-");


?> 