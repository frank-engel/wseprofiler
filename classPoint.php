<?php
class Point
{
  var $x, $y;
  function __construct( $x, $y ) { $this->x = $x; $this->y = $y; }
};

function BilinearInterpolate( $x, $a, $b ) // $a and $b are two points, to be interpolated at position $x
{
  $s = ($b->y - $a->y) / ($b->x - $a->x);
  return $a->y + $s * ($x - $a->x);
}

function LinearInterpolate( $xi, $x1, $y1, $x2, $y2 ) // $a and $b are two points, to be interpolated at position $x
{
  return ($xi - $x1) * ($y2 - $y1) / ($x2 - $x1) + $y1;
}



$x = 5;
$y = LinearInterpolate($x,3,1,8,4);
print ("$x , $y"); // outputs 5 , 2.2
//print_r($p);
?>