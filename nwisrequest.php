<?php
function read_nwis($station,$startdate,$enddate,$parameter){
// Validation
if (!isset($station) || empty($station)) {
	break;
};

// Increase the memory limit to help avoid
// out of memory errors
ini_set('memory_limit', '1024M'); 

// Read some RDB format data using a URL query to NWIS Web (2 parameter)
//Is this is the long processing code? If so, writing a chron job might be more complex since $startdate and $enddate are established on the site.
$url = "http://nwis.waterdata.usgs.gov/usa/nwis/uv?".$parameter."=on&format=rdb&period=&begin_date=".$startdate."&end_date=".$enddate."&site_no=".$station."&referred_module=sw";
$rdb=file_get_contents($url);

//echo '<p><a href="'.$url.'">'.$url.'</a></p><br/>';

// Separate the data into an Array of rows
$rows = explode("\n", $rdb);

// Parse the RDB, and deal to variables
// First, traverse the header. In no case should the header
// be longer than 100 rows, so use that as the max for loop size
$maxHeaderSize = 100;
for ($offset = 0; $offset <= $maxHeaderSize; $offset++){
    // Take header row by row. Stop when the first character 
	// is no longer "#"
	$irow = trim($rows[$offset]);
	$irow = preg_replace('/\s+/',' ',$irow);
	$icol = explode(" ", $irow);
	if (!in_array("#",$icol)){
		break; 
	} 
}

// Segment the RDB Data, Labels, and Comment Codes
$colnames = array_slice($rows,$offset,1);
$colcomments = array_slice($rows,$offset+1,1);
$data = array_slice($rows,$offset+2);

/*
echo '<p>nwisrequest: The loaded RDB webquery contains the following columns:</p>';
echo "<pre>";
print_r($colnames);
echo "</pre>";
*/

// Parse each row
$i = 0;
foreach($data as $key => $value){
    $cols[$i] = explode("\t", $value);
    $i++;
}

// Extract discharge and time
$i = 0;

$rawtime   = array();
$outparam = array();
foreach ($cols as $key1 => $value1) {
	foreach ($value1 as $key2 => $value2) {
		if ($key2 == 2 && !empty($value2)) {
			$rawtime[$i] = $value2;
		}
		if ($key2 == 4 && !empty($value2)) {
			$outparam[$i] = $value2;
		$i++;
		}
	}
}
while (count($rawtime)>count($outparam)){
	array_pop($rawtime);
}

$i = count($rawtime);
$times = array();
for ($ii = 0; $ii < $i; $ii++){
	$times[$ii] = strtotime($rawtime[$ii]);
}
while (count($times)>count($outparam)){
	array_pop($times);
}

return array($times,$outparam);

}
?>