<?php
// Load and read server stored rdb file and output array
//function read_nwis($filename){
// Increase the memory limit to help avoid
// out of memory errors
ini_set('memory_limit', '1024M'); 

$filename="nwisdata/Q05536890.dat";

// Load the data into array of rows
echo 'Start: date("Y-m-d H:i:s")';
$rdb = file_get_contents($filename);
echo 'End: date("Y-m-d H:i:s")';
$rows = explode("\n", $rdb);

// Parse the RDB, and deal to variables
// First, traverse the header. In no case should the header
// be longer than 100 rows, so use that as the max for loop size
$maxHeaderSize = 100;
for ($offset = 0; $offset <= $maxHeaderSize; $offset++){
    // Take header row by row. Stop when the first character 
	// is no longer "#"
	$irow = trim($rows[$offset]);
	$irow = preg_replace('/\s+/',' ',$irow);
	$icol = explode(" ", $irow);
	if (!in_array("#",$icol)){
		break; 
	} 
}

// Segment the RDB Data, Labels, and Comment Codes
$colnames = array_slice($rows,$offset,1);
$colcomments = array_slice($rows,$offset+1,1);
$data = array_slice($rows,$offset+2);

// Parse each row
$i = 0;
foreach($data as $key => $value){
    $cols[$i] = explode("\t", $value);
    $i++;
}

// Extract discharge and time
$i = 0;
$rawtime   = array();
$outparam = array();
foreach ($cols as $key1 => $value1) {
	foreach ($value1 as $key2 => $value2) {
		if ($key2 == 2 && !empty($value2)) {
			$rawtime[$i] = $value2;
		}
		if ($key2 == 4 && !empty($value2)) {
			$outparam[$i] = $value2;
		$i++;
		}
	}
}
while (count($rawtime)>count($outparam)){
	array_pop($rawtime);
}

$i = count($rawtime);
$times = array();
for ($ii = 0; $ii < $i; $ii++){
	$times[$ii] = strtotime($rawtime[$ii]);
}
while (count($times)>count($outparam)){
	array_pop($times);
}
//var_dump($outparam);
return array($times,$outparam);
//}
?>